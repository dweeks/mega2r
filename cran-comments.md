In this update, '\docType{package}' was replaced with "_PACKAGE" as now required.

- using R version 4.3.2 (2023-10-31)
- using platform: aarch64-apple-darwin20 (64-bit)
Status: OK

* using R Under development (unstable) (2023-10-30 r85440)
* using platform: aarch64-apple-darwin20
Status: OK
R CMD check succeeded

-------

## Previous CRAN removal

Does not use Suggests: TxDb.Hsapiens.UCSC.hg19.knownGene conditionally.

    Quitting from lines 694-696 (mega2rtutorial.Rmd)
    Error: processing vignette 'mega2rtutorial.Rmd' failed with diagnostics:
    there is no package called 'TxDb.Hsapiens.UCSC.hg19.knownGene'


Note that someone wanting to run the examples/tests/vignettes may not have a suggested package available (and it may not even be possible to install it for that platform). The recommendation used to be to make their use conditional via if(require("pkgname")): this is OK if that conditioning is done in examples/tests/vignettes, although using if(requireNamespace("pkgname")) is preferred, if possible.

## R packages

GenABEL has been archived.

## Testing

R CMD build mega2r
R CMD check --as-cran Mega2R_1.0.9.tar.gz

To test using the WinBuilder machines:

$ ftp ftp://win-builder.r-project.org/
Connected to win-builder.r-project.org.
220-Microsoft FTP Service
220 Welcome on win-builder.R-project.org
331 Anonymous access allowed, send identity (e-mail name) as password.
230-Before uploading source packages to R-release, R-devel please read
    https://win-builder.R-project.org/index.htm
230 User logged in.
Remote system type is Windows_NT.
200 Type set to I.
ftp> cd R-release
ftp> binary
ftp> put Mega2R_1.0.9.tar.gz
ftp> quit

## Test environment
* OS X 11.6, R devel version (2021-11-28 r81257) -- "Unsuffered Consequences"
* Platform: x86_64-w64-mingw32 (64-bit) using R Under development (unstable) (2021-12-03 r81290)
 
## R CMD check results
On both OS X and WinBuilder, there were no ERRORs or WARNINGs.

There was one NOTE, which has been addressed in this update:

* checking CRAN incoming feasibility ... NOTE
Maintainer: ‘Daniel E. Weeks <weeks@pitt.edu>’

New submission

Package was archived on CRAN

CRAN repository db overrides:
  X-CRAN-Comment: Archived on 2021-04-06 as check problems were not
    corrected in time.

  Does not use Suggests: TxDb.Hsapiens.UCSC.hg19.knownGene
    conditionally.




