## Mega2R Bitbucket repository ##

Mega2R is an R package that makes it easy to load SQLite databases
created by Mega2 directly into R as data frames.  It also provides
support for carrying out gene-based association tests, automatically
looping over genes, using a variety of other R packages.  For more
information about Mega2R, see this web page:
<https://watson.hgen.pitt.edu/mega2/mega2r/>

The latest development snapshot of the Mega2R R package can 
be obtained from this Bitbucket repository.  Please note
that this development snapshot is not as thoroughly tested
as our stable release version, but does contain the newest
features and changes.

To obtain the stable release version, please go to
<https://CRAN.R-project.org/package=Mega2R>

## Contact ##

Daniel E. Weeks, Ph.D.  
Professor of Human Genetics and Biostatistics   
Department of Human Genetics   
University of Pittsburgh   
Public Health 3119   
130 DeSoto Street   
Pittsburgh, PA 15261   
USA   

Work: 1 412 624 5388  
Email: weeks@pitt.edu  
Web site: https://watson.hgen.pitt.edu  
Twitter: @StatGenDan  
